const constants = require("../constants/constants");
const Gateway = require("fabric-network").Gateway;
const fs = require('fs');

/**
 * Create a connection to the network
 * @param wallet Wallet
 * @return JSONObject HF network and gateway instances
 */
async function getConnection(wallet) {
    const userName = constants.USER_NAME;
    const channelName = constants.CHANNEL_NAME;

    const ccpPath = constants.PEER_CONNECTION_CONFIGURATION_FILE;
    const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

    // Check to see if we've already enrolled the user.
    const identity = await wallet.get(userName);
    if (!identity) {
        console.log('An identity for the user "' + userName + '" does not exist in the wallet');
        return;
    }

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    await gateway.connect(ccp, {wallet, identity: userName, discovery: {enabled: true, asLocalhost: true}});

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork(channelName);

    // Get the contract from the network.
    return {
        network: network,
        gateway: gateway
    };
}

module.exports = {
    getConnection
};
