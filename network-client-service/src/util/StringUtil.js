'use strict';

/**
 * Binary to string conversion
 * @param array Binary[] binary array to convert
 * @return String version of given binary array
 */
function bin2string(array) {
    let result = "";
    for (let i = 0; i < array.length; ++i) {
        result += (String.fromCharCode(array[i]));
    }
    return result;
}

module.exports.bin2string = bin2string;
