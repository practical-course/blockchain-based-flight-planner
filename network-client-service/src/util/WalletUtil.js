const constants = require("../constants/constants");
const Wallets = require("fabric-network/lib/impl/wallet/wallets").Wallets;
const path = require('path');

/**
 * Create a new file system wallet
 */
async function getWallet() {
    // Create a new file system based wallet for managing identities.
    const walletPath = path.resolve(constants.WALLET_PATH);
    return await Wallets.newFileSystemWallet(walletPath);
}

module.exports = {
    getWallet
};
