'use strict';

const path = require('path');

// System wide constants
const PEER_CONNECTION_CONFIGURATION_FILE = path.resolve(__dirname, '..', '..', '..', 'uav-management-network', 'organizations', 'peerOrganizations', 'org1.example.com', 'connection-org1.json');
const USER_NAME = 'admin';
const WALLET_PATH = path.resolve(__dirname, '..', '..', 'wallet');
const CHAINCODE_NAME = 'flightplan';
const CHANNEL_NAME = 'mychannel';
const SERVER_PORT = 9000;

module.exports = {
    WALLET_PATH, SERVER_PORT, PEER_CONNECTION_CONFIGURATION_FILE, USER_NAME, CHAINCODE_NAME, CHANNEL_NAME
};
