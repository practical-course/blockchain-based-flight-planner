'use strict';

const express = require('express');
const app = express();
const router = express.Router();
const cors = require('cors');
const FlightPlanService = require('../service/FlightPlanService');
const bodyParser = require('body-parser');

// web server attributes
app.set('caseSensitive', true);
app.use(cors());
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

/**
 * Flight plan submission API
 * @param key String: flight plan key/number
 * @param flightId String
 * @param height Integer: How high is the UAV flying
 * @param startLatitude Float
 * @param endLatitude Float
 * @param startLongitude Float
 * @param endLongitude Float
 * @param ownerId String: ID of the UAV owner
 * @param time DateTime: When the UAV will fly
 */
router.post('/', (req, res) => {
    console.log("Req: ", req.body);
    console.log(req.body.key, req.body.uavId, req.body.flightId, req.body.height,
        req.body.startLatitude, req.body.startLongitude, req.body.endLatitude, req.body.endLongitude, req.body.ownerId, req.body.time);
    FlightPlanService.submitFlightPlan(req.body.key, req.body.uavId, req.body.flightId, req.body.height,
        req.body.startLatitude, req.body.startLongitude, req.body.endLatitude, req.body.endLongitude, req.body.ownerId, req.body.time);
    res.sendStatus(200);
});

/**
 * Flight plan lister APU
 * @param historicData Boolean: does user requested valid or historic data
 * @return list of valid or historic data
 */
router.get('/', async (req, res) => {
    console.log("Req: ", req.body);
    const getHistoricData = req.query.historicData;
    let historicalFPData;
    if (getHistoricData) {
        historicalFPData = await FlightPlanService.getFlightPlans(true)
    } else {
        historicalFPData = await FlightPlanService.getFlightPlans(false);
    }

    if (historicalFPData === null) {
        res.status(500).send({'message': 'Something went wrong'});
        return;
    }

    res.status(200).send({'body': historicalFPData});
});

module.exports = router;
