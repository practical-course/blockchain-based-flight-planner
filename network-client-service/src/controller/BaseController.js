const router = require('express').Router();

// attach routers
router.use('/flightplan', require('./FlightPlanController'));
router.use('/airspace', require('./AirspaceRegulationController'));

module.exports = router;
