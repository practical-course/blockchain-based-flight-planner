'use strict';

const express = require('express');
const app = express();
const router = express.Router();
const cors = require('cors');
const AirSpaceRegulationService = require('../service/AirSpaceRegulationService');
const bodyParser = require('body-parser');

// web server attributes
app.set('caseSensitive', true);
app.use(cors());
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

/**
 * Airspace regulation creation API. Params are inside req.body
 */
router.post('/', (req, res) => {
    AirSpaceRegulationService.submitAirspaceRegulation(req.body.key, req.body.attributeName, req.body.operation, req.body.threshold, req.body.startLatitude, req.body.startLongitude,
        req.body.endLatitude, req.body.endLongitude, req.body.startDateTime, req.body.endDateTime);
    res.sendStatus(200);
});

/**
 * Airspace regulation lister API.
 */
router.get('/', async (req, res) => {
    console.log("Req: ", req.body);
    const historicData = req.query.historicData;
    let historicalFPData;

    if (historicData) {
        historicalFPData = await AirSpaceRegulationService.getAirspaceRegulations(false);
    } else {
        historicalFPData = await AirSpaceRegulationService.getAirspaceRegulations(true);
    }

    if (historicalFPData === null) {
        res.status(500).send({'message': 'Something went wrong'});
        return;
    }

    res.status(200).send({'body': historicalFPData});
});

module.exports = router;
