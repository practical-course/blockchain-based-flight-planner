const app = require('express')();
const constants = require('../constants/constants');
const UserService = require('../service/UserService');
const cors = require('cors');
const bodyParser = require('body-parser');

app.set('caseSensitive', true);
app.use(cors());
app.use(bodyParser());


// mount the router on the app
app.use('/api', require('../controller/BaseController'));

/**
 * Listen on predefined port
 */
app.listen(constants.SERVER_PORT, async () => {
    try {
        // enroll pre-registered admin user to org1
        await UserService.enrollAdmin('org1', 'Org1MSP', 'admin');
    } catch (e) {
        console.log(e);
    }

    console.log("Listening on port ", constants.SERVER_PORT, "...");
});

module.exports = app;
