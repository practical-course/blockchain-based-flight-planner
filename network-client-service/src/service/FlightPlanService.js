'use strict';

const PeerConnectionUtil = require('../util/PeerConnectionUtil');
const WalletUtil = require('../util/WalletUtil');
const StringUtil = require('../util/StringUtil');

const constants = require('../constants/constants');

/**
 * Submits a new flight plan
 * @param key String: flight plan key
 * @param uavId String
 * @param flightId String
 * @param height Integer: how high the UAV will fly
 * @param startLatitude Float
 * @param startLongitude Float
 * @param endLatitude Float
 * @param endLongitude Float
 * @param ownerId String
 * @param time DateTime: When the UAV will fly
 */
async function submitFlightPlan(key, uavId, flightId, height, startLatitude, startLongitude, endLatitude, endLongitude, ownerId, time) {
    try {
        // create file based walled instance and connect to the network
        const wallet = await WalletUtil.getWallet();
        const peerConnection = await PeerConnectionUtil.getConnection(wallet);

        // get flight plan contract instance
        const contract = await peerConnection.network.getContract(constants.CHAINCODE_NAME, 'FlightPlan');

        // submit flight plan
        await contract.submitTransaction('createFlightPlan', key, flightId, height, uavId, startLatitude, startLongitude, endLatitude, endLongitude, ownerId, time);
        console.log('Transaction has been submitted');

        // Disconnect from the gateway.
        await peerConnection.gateway.disconnect();
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
    }
}

/**
 * Get all valid or old valid plans depending on the input
 * @param oldDataRequested Boolean: true if historic data requested
 * @return List of active flight plan in case of "true" input, historic flight plans otherwise
 */
async function getFlightPlans(oldDataRequested) {
    let historicalFlightPlans = [];

    try {
        // create file based walled instance and connect to the network
        const wallet = await WalletUtil.getWallet();
        const peerConnection = await PeerConnectionUtil.getConnection(wallet);

        // get flight plan contract instance
        const contract = await peerConnection.network.getContract(constants.CHAINCODE_NAME, 'FlightPlan');

        // call queryAllFlightPlans method on chaincode and parse the output
        const response = await contract.submitTransaction("queryAllFlightPlans");
        const allFlightPlans = JSON.parse(StringUtil.bin2string(response));

        if (oldDataRequested) {
            // get only old flight plans in case historic data requested
            for (let i = 0; i < allFlightPlans.length; i++) {
                if (Date.parse(allFlightPlans[i].Record.time) < Date.now()) {
                    historicalFlightPlans.push(reformatFlightplan(allFlightPlans[i]));
                }
            }
        } else {
            for (let i = 0; i < allFlightPlans.length; i++) {
                if (Date.parse(allFlightPlans[i].Record.time) > Date.now()) {
                    historicalFlightPlans.push(reformatFlightplan(allFlightPlans[i]));
                }
            }
        }

        for (let flightplan in allFlightPlans) {

        }

    } catch (error) {
        console.error(`Failed to get historical flight plans: ${error}`);
        return null;
    }

    return historicalFlightPlans;
}

function reformatFlightplan(flightPlan) {
    let formattedFlightPlan = flightPlan.Record;

    formattedFlightPlan['startLocation'] =
        formattedFlightPlan['startLatitude'] + ":" + formattedFlightPlan['startLongitude'];
    formattedFlightPlan['endLocation'] =
        formattedFlightPlan['endLatitude'] + ":" + formattedFlightPlan['endLongitude'];

    delete formattedFlightPlan['docType'];
    delete formattedFlightPlan['startLatitude'];
    delete formattedFlightPlan['startLongitude'];
    delete formattedFlightPlan['endLatitude'];
    delete formattedFlightPlan['endLongitude'];

    return formattedFlightPlan;
}

module.exports = {
    submitFlightPlan, getFlightPlans,
};
