'use strict';

const {Wallets} = require('fabric-network');
const FabricCAServices = require('fabric-ca-client');
const fs = require('fs');
const path = require('path');
const constants = require('../constants/constants');

/**
 * Enroll an admin user to the given org
 * @param orgId String: organization ID
 * @param mspId String: membership service provider ID
 * @param userName String
 */
async function enrollAdmin(orgId, mspId, userName) {
    try {
        // load the network configuration
        const ccpPath = constants.PEER_CONNECTION_CONFIGURATION_FILE;
        const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

        // Create a new CA client for interacting with the CA.
        const caInfo = ccp.certificateAuthorities['ca.' + orgId + '.example.com'];
        const caTLSCACerts = caInfo.tlsCACerts.pem;
        const ca = new FabricCAServices(caInfo.url, {trustedRoots: caTLSCACerts, verify: false}, caInfo.caName);

        // Create a new file system based wallet for managing identities.
        const walletPath = path.resolve(constants.WALLET_PATH);
        const wallet = await Wallets.newFileSystemWallet(walletPath);

        // Check to see if we've already enrolled the admin user.
        const identity = await wallet.get(userName);
        if (identity) {
            console.log('An identity for the admin user "' + userName + '" already exists in the wallet');
            return;
        }

        // Enroll the admin user, and import the new identity into the wallet.
        const enrollment = await ca.enroll({enrollmentID: userName, enrollmentSecret: userName + 'pw'});
        const x509Identity = {
            credentials: {
                certificate: enrollment.certificate,
                privateKey: enrollment.key.toBytes(),
            },
            mspId: mspId,
            type: 'X.509',
        };
        await wallet.put(userName, x509Identity);
        console.log('Successfully enrolled admin user "' + userName + '" and imported it into the wallet');

    } catch (error) {
        console.error(`Failed to enroll admin user "` + userName + `": ${error}`);
    }
}

/**
 * Register given user
 * @param orgId String: organization ID
 * @param mspId String: membership service provider ID
 * @param userName String
 * @param adminUserName String: username of an admin user in the same organization
 */
async function registerUser(orgId, mspId, userName, adminUserName) {
    try {
        // load the network configuration
        const ccpPath = constants.PEER_CONNECTION_CONFIGURATION_FILE;
        const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

        // Create a new CA client for interacting with the CA.
        const caURL = ccp.certificateAuthorities['ca.' + orgId + '.example.com'].url;
        const ca = new FabricCAServices(caURL);

        // Create a new file system based wallet for managing identities.
        const walletPath = path.resolve(constants.WALLET_PATH);
        const wallet = await Wallets.newFileSystemWallet(walletPath);

        // Check to see if we've already enrolled the user.
        const userIdentity = await wallet.get(userName);
        if (userIdentity) {
            console.log('An identity for the user "' + userName + '" already exists in the wallet');
            return;
        }

        // Check to see if we've already enrolled the admin user.
        const adminIdentity = await wallet.get(adminUserName);
        if (!adminIdentity) {
            console.log('An identity for the admin user "' + adminUserName + '" does not exist in the wallet');
            return;
        }

        // build a user object for authenticating with the CA
        const provider = wallet.getProviderRegistry().getProvider(adminIdentity.type);
        const adminUser = await provider.getUserContext(adminIdentity, adminUserName);

        // Register the user, enroll the user, and import the new identity into the wallet.
        const secret = await ca.register({
            affiliation: orgId + '.department1',
            enrollmentID: userName,
            role: 'client'
        }, adminUser);
        const enrollment = await ca.enroll({
            enrollmentID: userName,
            enrollmentSecret: secret
        });
        const x509Identity = {
            credentials: {
                certificate: enrollment.certificate,
                privateKey: enrollment.key.toBytes(),
            },
            mspId: mspId,
            type: 'X.509',
        };
        await wallet.put(userName, x509Identity);
        console.log('Successfully registered and enrolled admin user "' + userName + '" and imported it into the wallet');

    } catch (error) {
        console.error(`Failed to register user "` + userName + `": ${error}`);
    }
}

module.exports = {
    enrollAdmin, registerUser
};
