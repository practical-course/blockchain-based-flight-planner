'use strict';

const PeerConnectionUtil = require('../util/PeerConnectionUtil');
const WalletUtil = require('../util/WalletUtil');
const StringUtil = require('../util/StringUtil');

const constants = require('../constants/constants');

/**
 * Airspace regulation submitter service
 * @param key String
 * @param attributeName String: Name of the attribute of flight plan to regulate
 * @param operation String: Regulation operation, e.g. < or >
 * @param threshold String: threshold value of the regulation, can be an int or string
 * @param startLatitude Float: start latitude of rectangular area where the regulation is valid
 * @param startLongitude Float: start longitude of rectangular area where the regulation is valid
 * @param endLatitude Float: end latitude of rectangular area where the regulation is valid
 * @param endLongitude Float: end longitude of rectangular area where the regulation is valid
 * @param startDateTime String: start date/time of the regulation
 * @param endDateTime String: end date/time of the regulation
 */
async function submitAirspaceRegulation(key, attributeName, operation, threshold, startLatitude, startLongitude,
                                        endLatitude, endLongitude, startDateTime, endDateTime) {
    try {
        // create file based walled instance and connect to the network
        const wallet = await WalletUtil.getWallet();
        const peerConnection = await PeerConnectionUtil.getConnection(wallet);

        // get flight plan contract instance
        const contract = await peerConnection.network.getContract(constants.CHAINCODE_NAME, 'AirspaceRegulation');

        // submit the request
        await contract.submitTransaction('createAirspaceRegulation', key, attributeName, operation, threshold, startLatitude, startLongitude,
            endLatitude, endLongitude, startDateTime, endDateTime);
        console.log('Transaction has been submitted');

        // Disconnect from the gateway.
        await peerConnection.gateway.disconnect();
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
    }
}

/**
 * Get list of valid/historic airspace regulations
 * @param historicData Boolean: does user requests valid data or historic data
 * @return list of airspace regulations
 */
async function getAirspaceRegulations(historicData) {
    let airSpaceRegulationList = [];

    try {
        // create file based walled instance and connect to the network
        const wallet = await WalletUtil.getWallet();
        const peerConnection = await PeerConnectionUtil.getConnection(wallet);

        // get flight plan contract instance
        const contract = await peerConnection.network.getContract(constants.CHAINCODE_NAME, 'AirspaceRegulation');

        // submit transaction and parse response
        const response = await contract.submitTransaction("queryAllAirspaceRegulations");
        const allAirspaceRegulations = JSON.parse(StringUtil.bin2string(response));

        // get only historic/valid data
        if (historicData) {
            for (let i = 0; i < allAirspaceRegulations.length; i++) {
                if (Date.parse(allAirspaceRegulations[i].Record.endDateTime) > Date.now()) {
                    airSpaceRegulationList.push(reformatAirspaceRegulation(allAirspaceRegulations[i]));
                }
            }
        } else {
            for (let i = 0; i < allAirspaceRegulations.length; i++) {
                if (Date.parse(allAirspaceRegulations[i].Record.endDateTime) < Date.now()) {
                    airSpaceRegulationList.push(reformatAirspaceRegulation(allAirspaceRegulations[i]));
                }
            }
        }
    } catch (error) {
        console.error(`Failed to get historical flight plans: ${error}`);
        return null;
    }

    return airSpaceRegulationList;
}

function reformatAirspaceRegulation(airspaceRegulation){
    let formattedAirspaceRegulation = airspaceRegulation.Record;

    formattedAirspaceRegulation['startLocation'] =
        formattedAirspaceRegulation['startLatitude'] + ":" + formattedAirspaceRegulation['startLongitude'];
    formattedAirspaceRegulation['endLocation'] =
        formattedAirspaceRegulation['endLatitude'] + ":" + formattedAirspaceRegulation['endLongitude'];

    delete formattedAirspaceRegulation['docType'];
    delete formattedAirspaceRegulation['startLatitude'];
    delete formattedAirspaceRegulation['startLongitude'];
    delete formattedAirspaceRegulation['endLatitude'];
    delete formattedAirspaceRegulation['endLongitude'];

    return formattedAirspaceRegulation;

}

module.exports = {
    submitAirspaceRegulation, getAirspaceRegulations,
};
