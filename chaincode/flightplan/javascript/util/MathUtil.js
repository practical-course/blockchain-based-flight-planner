/**
 * Implementation is taken from: https://stackoverflow.com/a/24392281/8739916
 * Check if given two line intersects
 * Line 1: (a,b) -> (c,d), Line 2: (p,q) -> (r,s)
 * @param a int
 * @param b int
 * @param c int
 * @param d int
 * @param p int
 * @param q int
 * @param r int
 * @param s int
 * @return boolean true if they intersects, false otherwise
 */
function isTwoLineIntersects(a, b, c, d, p, q, r, s) {
    var det, gamma, lambda;
    // calculate determinant
    det = (c - a) * (s - q) - (r - p) * (d - b);
    if (det === 0) {
        // det 0 means no intersect
        return false;
    } else {
        // If lambda and gamma both between (0,1), this means they intersect
        lambda = ((s - q) * (r - a) + (p - r) * (s - b)) / det;
        gamma = ((b - d) * (r - a) + (c - a) * (s - b)) / det;
        return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
    }
}

module.exports.isTwoLineIntersects = isTwoLineIntersects;
