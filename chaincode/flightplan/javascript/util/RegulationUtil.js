'use strict';

const mathUtil = require('./MathUtil');

/**
 * Apply given airspace regulations to the given flight plan
 * @param flightPlan JSONObject: represents a flight plan transaction
 * @param regulationList JSONArray: list of airspace regulations
 * @return list of regulations that given flight plan violates or null if there is not any
 * */
async function applyRegulations(flightPlan, regulationList) {
    let violationList = [];
    let regulationJsonArray = JSON.parse(regulationList);

    // compare given flight plan against each of the given regulations
    for (let i = 0; i < regulationJsonArray.length; i++) {
        let regulation = regulationJsonArray[i];
        // check if flight plan is between regulation time
        if (!(Date.parse(flightPlan.time) > Date.parse(regulation.Record.startDateTime) && Date.parse(flightPlan.time) < Date.parse(regulation.Record.endDateTime))) {
            console.log("Given flight plan date is not between regulation date!");
            continue;
        }

        let flightPlanStartLatitude = parseFloat(flightPlan.startLatitude);
        let flightPlanStartLongitude = parseFloat(flightPlan.startLongitude);
        let flightPlanEndLatitude = parseFloat(flightPlan.endLatitude);
        let flightPlanEndLongitude = parseFloat(flightPlan.endLongitude);

        let airspacRegulationStartLatitude = parseFloat(regulation.Record.startLatitude);
        let airspacRegulationStartLongitude = parseFloat(regulation.Record.startLongitude);
        let airspacRegulationEndLatitude = parseFloat(regulation.Record.endLatitude);
        let airspacRegulationEndLongitude = parseFloat(regulation.Record.endLongitude);

        // check if given flight plan route (accept it as a straight line) intersects with any of the rectangular shaped airspace regulation area's lines
        if (!(mathUtil.isTwoLineIntersects(flightPlanStartLatitude, flightPlanStartLongitude, flightPlanEndLatitude, flightPlanEndLongitude,
            airspacRegulationStartLatitude, airspacRegulationStartLongitude, airspacRegulationStartLatitude, airspacRegulationEndLongitude) ||
            mathUtil.isTwoLineIntersects(flightPlanStartLatitude, flightPlanStartLongitude, flightPlanEndLatitude, flightPlanEndLongitude,
                airspacRegulationStartLatitude, airspacRegulationStartLongitude, airspacRegulationEndLatitude, airspacRegulationStartLongitude) ||
            mathUtil.isTwoLineIntersects(flightPlanStartLatitude, flightPlanStartLongitude, flightPlanEndLatitude, flightPlanEndLongitude,
                airspacRegulationEndLatitude, airspacRegulationEndLongitude, airspacRegulationEndLatitude, airspacRegulationStartLongitude) ||
            mathUtil.isTwoLineIntersects(flightPlanStartLatitude, flightPlanStartLongitude, flightPlanEndLatitude, flightPlanEndLongitude,
                airspacRegulationEndLatitude, airspacRegulationEndLongitude, airspacRegulationStartLatitude, airspacRegulationEndLongitude))) {

            // if not, finally check if given route is inside the rectangular shaped airspace regulation area
            if (!(flightPlanStartLatitude > airspacRegulationStartLatitude && flightPlanEndLatitude < airspacRegulationEndLatitude
                && flightPlanStartLongitude > airspacRegulationStartLongitude && flightPlanEndLatitude < airspacRegulationEndLongitude
                && flightPlanEndLatitude > airspacRegulationStartLatitude && flightPlanEndLatitude < airspacRegulationEndLatitude
                && flightPlanEndLongitude > airspacRegulationStartLongitude && flightPlanEndLongitude < airspacRegulationEndLongitude)) {
                // if also route of the flight plan is not inside the rectangular area, this means this regulation is not
                // related with given flight plan
                continue;
            }
        }

        let operation = regulation.Record.operation;
        let attribute = regulation.Record.attributeName;
        let threshold = regulation.Record.threshold;
        switch (operation) {
            // check the airspace regulation operation and apply it with threshold
            // store violations inside violation list
            case '<':
                if (flightPlan[attribute] >= threshold) {
                    violationList.push(regulation);
                    continue;
                }
                break;
            case ">":
                if (flightPlan[attribute] <= threshold) {
                    violationList.push(regulation);
                    continue;
                }
                break;
            case "=":
                if (flightPlan[attribute] !== threshold) {
                    violationList.push(regulation);
                    continue;
                }
                break;
            case "!=":
                if (flightPlan[attribute] === threshold) {
                    violationList.push(regulation);
                    continue;
                }
                break;
            default:
                break;
        }
    }

    // return violation list if any
    if (violationList.length > 0) {
        return violationList;
    }

    return null;
}

module.exports.applyRegulations = applyRegulations;
