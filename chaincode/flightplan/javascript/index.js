/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const FlightPlan = require('./contract/flightplan');
const AirspaceRegulation = require('./contract/airspaceregulation');

module.exports.FlightPlan = FlightPlan;
module.exports.AirspaceRegulation = AirspaceRegulation;
module.exports.contracts = [ FlightPlan, AirspaceRegulation ];
