/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const {Contract} = require('fabric-contract-api');
const AirspaceRegulation = require('./airspaceregulation');
const RegulationUtil = require('../util/RegulationUtil');

/**
 * Flight plan contract
 */
class FlightPlan extends Contract {

    /**
     * Initially put 5 flight plans into db for test purposes
     * */
    async initFlightPlans(ctx) {
        console.info('============= START : Initialize Ledger ===========');
        // pack 5 test flight plans
        const flightPlans = [
            {
                uavId: 'uavId01',
                flightId: 'flightId01',
                height: '1000',
                startLatitude: '44,1233',
                startLongitude: '44,1233',
                endLatitude: '46,123',
                endLongitude: '46,123',
                ownerId: 'ownerId01',
                time: '2020-01-01 12:00:00'
            },
            {
                uavId: 'uavId02',
                flightId: 'flightId02',
                height: '1000',
                startLatitude: '44,1233',
                startLongitude: '44,1233',
                endLatitude: '46,123',
                endLongitude: '46,123',
                ownerId: 'ownerId02',
                time: '2020-01-01 13:00:00'
            },
            {
                uavId: 'uavId03',
                flightId: 'flightId03',
                height: '1000',
                startLatitude: '44,1233',
                startLongitude: '44,1233',
                endLatitude: '46,123',
                endLongitude: '46,123',
                ownerId: 'ownerId03',
                time: '2020-01-01 14:00:00'
            },
            {
                uavId: 'uavId04',
                flightId: 'flightId04',
                height: '1000',
                startLatitude: '44,1233',
                startLongitude: '44,1233',
                endLatitude: '46,123',
                endLongitude: '46,123',
                ownerId: 'ownerId04',
                time: '2020-01-01 15:00:00'
            },
            {
                uavId: 'uavId05',
                flightId: 'flightId06',
                height: '1000',
                startLatitude: '44,1233',
                startLongitude: '44,1233',
                endLatitude: '46,123',
                endLongitude: '46,123',
                ownerId: 'ownerId05',
                time: '2020-01-01 16:00:00'
            }
        ];

        // put flight plans into db with "flightplan" doctype
        for (let i = 0; i < flightPlans.length; i++) {
            flightPlans[i].docType = 'flightPlan';
            await ctx.stub.putState('FLIGHTPLAN' + i, Buffer.from(JSON.stringify(flightPlans[i])));
            console.info('Added <--> ', flightPlans[i]);
        }
        console.info('============= END : Initialize Ledger ===========');
    }

    /**
     * Query a flight plan by its key
     * @param ctx Contract.Context: contract context which contains all identity info of the client along with chain code stub
     * @param flightPlanNumber String
     * @return Flight plan with given flight plan number
     * */
    async queryFlightPlan(ctx, flightPlanNumber) {
        console.info('============= START : Query Single Flight Plan ===========');

        // search given flightPlanNumber in ledger
        const flightPlanAsBytes = await ctx.stub.getState(flightPlanNumber);
        if (!flightPlanAsBytes || flightPlanAsBytes.length === 0) {
            throw new Error(`${flightPlanNumber} does not exist`);
        }

        return flightPlanAsBytes.toString();
    }

    /**
     * Create a flight plan with given parameters
     * @param ctx Contract.Context: contract context which contains all identity info of the client along with chain code stub
     * @param flightPlanNumber String
     * @param flightId String
     * @param height Integer: How high is the UAV flying
     * @param startLatitude Float
     * @param endLatitude Float
     * @param startLongitude Float
     * @param endLongitude Float
     * @param ownerId String: ID of the UAV owner
     * @param time DateTime: When the UAV will fly
     * */
    async createFlightPlan(ctx, flightPlanNumber, flightId, height, uavId, startLatitude, startLongitude, endLatitude, endLongitude, ownerId, time) {
        console.info('============= START : Create Flight Plan ===========');

        // pack given values
        const flightPlan = {
            uavId,
            flightId,
            height,
            startLatitude,
            startLongitude,
            endLatitude,
            endLongitude,
            ownerId,
            time,
            docType: 'flightPlan'
        };

        // get current airspace regulations
        const airspaceRegulations = await new AirspaceRegulation().queryAllAirspaceRegulations(ctx);
        // check if given flight plan contradicts with airspace regulations
        const ruleViolation = await RegulationUtil.applyRegulations(flightPlan, airspaceRegulations);
        if (ruleViolation === null) {
            // if not, then submit it to the network
            await ctx.stub.putState(flightPlanNumber, Buffer.from(JSON.stringify(flightPlan)));
            console.info('============= END : Create Flight Plan ===========');
            return null;
        } else {
            // if yes, then return the rule violation
            console.error("Given flight plan doesn't satisfy following airspace regulations: ", ruleViolation);
            console.error("============= END : Given flight plan doesn't satisfy all airspace regulations ===========");
            return ruleViolation;
        }
    }

    /**
     * Query all flight plans
     * @param ctx Contract.Context: contract context which contains all identity info of the client along with chain code stub
     * @return List of all flight plans
     * */
    async queryAllFlightPlans(ctx) {
        console.info('============= START : Query ALL Flight Plans ===========');

        const startKey = 'FLIGHTPLAN0';
        const endKey = 'FLIGHTPLANZ';
        const allResults = [];

        // query all flight plans in the ledger, which has a key starting with "FLIGHTPLAN+number"
        for await (const {key, value} of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;
            try {
                record = JSON.parse(strValue);
            } catch (err) {
                console.log(err);
                record = strValue;
            }
            allResults.push({Key: key, Record: record});
        }
        return JSON.stringify(allResults);
    }
}

module.exports = FlightPlan;
