/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const {Contract} = require('fabric-contract-api');

/**
 * Airspace regulation contract
 */
class AirspaceRegulation extends Contract {

    /**
     * Initially put 2 regulations into the contract
     * @param ctx Contract.Context: contract context which contains all identity info of the client along with chain code stub
     */
    async initAirspaceRegulations(ctx) {
        console.info('============= START : Initialize Ledger ===========');
        const airspaceRegulations = [
            {
                attributeName: 'height',
                operation: '>',
                threshold: '1000',
                startLatitude: '10,1233',
                startLongitude: '44,1233',
                endLatitude: '15,123',
                endLongitude: '46,123',
                startDatetime: '2020-06-06 00:00:00',
                endDateTime: '2021-05-05 12:00:00'
            },
            {
                attributeName: 'height',
                operation: '<',
                threshold: '2000',
                startLatitude: '44,1233',
                startLongitude: '44,1233',
                endLatitude: '48,123',
                endLongitude: '46,123',
                startDatetime: '2020-06-06 00:00:00',
                endDateTime: '2021-05-05 12:00:00'
            }
        ];

        // doctype for air space regulation contract is "airspaceregulation"
        for (let i = 0; i < airspaceRegulations.length; i++) {
            airspaceRegulations[i].docType = 'airspaceregulation';
            await ctx.stub.putState('AIRSPACEREGULATION' + i, Buffer.from(JSON.stringify(airspaceRegulations[i])));
            console.info('Added <--> ', airspaceRegulations[i]);
        }
        console.info('============= END : Initialize Ledger ===========');
    }

    /**
     * Query an airspace regulation by given key
     * @param ctx Contract.Context: contract context which contains all identity info of the client along with chain code stub
     * @param airspaceRegulationNumber String
     * @return airspace regulation with given key
     */
    async queryAirspaceRegulation(ctx, airspaceRegulationNumber) {
        console.info('============= START : Query Single Airspace Regulation ===========');

        // query the network with given key
        const airspaceRegulationAsBytes = await ctx.stub.getState(airspaceRegulationNumber);
        if (!airspaceRegulationAsBytes || airspaceRegulationAsBytes.length === 0) {
            throw new Error(`${airspaceRegulationNumber} does not exist`);
        }

        return airspaceRegulationAsBytes.toString();
    }

    /**
     * Create a new airspace regulation
     * @param ctx Contract.Context: contract context which contains all identity info of the client along with chain code stub
     * @param airspaceRegulationNumber String
     * @param attributeName String: Name of the attribute of flight plan to regulate
     * @param operation String: Regulation operation, e.g. < or >
     * @param threshold String: threshold value of the regulation, can be an int or string
     * @param startLatitude Float: start latitude of rectangular area where the regulation is valid
     * @param startLongitude Float: start longitude of rectangular area where the regulation is valid
     * @param endLatitude Float: end latitude of rectangular area where the regulation is valid
     * @param endLongitude Float: end longitude of rectangular area where the regulation is valid
     * @param startDateTime String: start date/time of the regulation
     * @param endDateTime String: end date/time of the regulation
     */
    async createAirspaceRegulation(ctx, airspaceRegulationNumber, attributeName, operation, threshold, startLatitude, startLongitude,
                                   endLatitude, endLongitude, startDateTime, endDateTime) {
        console.info('============= START : Create Airspace Regulation ===========');

        // pack regulation
        const airspaceRegulation = {
            docType: 'airspaceregulation',
            attributeName,
            operation,
            threshold,
            startLatitude,
            startLongitude,
            endLatitude,
            endLongitude,
            startDateTime,
            endDateTime
        };

        // save it without any condition
        await ctx.stub.putState(airspaceRegulationNumber, Buffer.from(JSON.stringify(airspaceRegulation)));
        console.info('============= END : Create Airspace Regulation ===========');
    }

    /**
     * Query all airspace regulations in the network
     * @param ctx Contract.Context: contract context which contains all identity info of the client along with chain code stub
     * @return list of all air space regulations
     */
    async queryAllAirspaceRegulations(ctx) {
        console.info('============= START : Query ALL Airspace Regulations ===========');

        const startKey = 'AIRSPACEREGULATION0';
        const endKey = 'AIRSPACEREGULATIONZ';
        const allResults = [];
        // get all the airspace regulations starting with "AIRSPACEREGULATION+number" key
        for await (const {key, value} of ctx.stub.getStateByRange(startKey, endKey)) {
            const strValue = Buffer.from(value).toString('utf8');
            let record;

            try {
                record = JSON.parse(strValue);
            } catch (err) {
                console.log(err);
                record = strValue;
            }

            allResults.push({Key: key, Record: record});
        }

        return JSON.stringify(allResults);
    }

}

module.exports = AirspaceRegulation;
