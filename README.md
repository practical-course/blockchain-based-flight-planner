## Useful Redirections

Network: https://gitlab.com/practical-course/blockchain-based-flight-planner/-/tree/master/uav-management-network<br />
Chaincode: https://gitlab.com/practical-course/blockchain-based-flight-planner/-/tree/master/chaincode/flightplan/javascript<br />
Web Service: https://gitlab.com/practical-course/blockchain-based-flight-planner/-/tree/master/network-client-service<br />
GUI: https://gitlab.com/practical-course/blockchain-based-flight-planner/-/tree/master/gui<br />


## How To Run

Note: In order to run the network and related services properly, do not move any folder or file.

#### Network

- Go into network root directory: `cd uav-management-network`
- Make automated network setup script runnable: `sudo chmod +x ./setup_network.sh`
- Run automated network setup script: `./setup_network.sh`

#### Web Service

- Go into web service root directory: `cd network-client-service`
- Install node packages: `npm install`
- Remove `wallet` folder that exists from previous network executions: `sudo rm -r wallet`. If this
is the first time you are running the network, or you didn't shutdown the network completely before,
then there should be no need for this command.
- Start web service: `node src/app/main.js`

#### GUI

- Go into GUI root directory: `cd gui`
- Install node packages: `npm install`
- Start GUI: `npm start`
